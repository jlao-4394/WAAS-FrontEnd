import Vue from "vue";
import Router from "vue-router";
import FrontPage from "../views/FrontPage.vue";
import SignUp from "../views/SignUp.vue";
import SignIn from "../views/SignIn.vue";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {path: '/', component: FrontPage},
        {path: '/signup', component: SignUp},
        {path: '/signin', component: SignIn}
    ]
})