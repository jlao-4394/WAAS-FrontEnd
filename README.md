# WAAS-FrontEnd

## Setup

``` bash
# npm
npm install

# yarn
yarn
```

## Run
```bash
# development
npm run dev

# production
npm run build
npm start
```